import React, { Component } from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { logoutUser } from "../../actions/authActions";
import axios from 'axios'
import { FilePond } from 'react-filepond';
import 'filepond/dist/filepond.min.css';

class Dashboard extends Component {

  constructor() {
    super();
    this.state = { selectedFile: null, loaded: 0, SHA: "nill" }
    this.sha_change = this.sha_change.bind(this);
  }

  sha_change(value){
    this.setState({SHA:value})
  }

  onLogoutClick = e => {
    e.preventDefault();
    this.props.logoutUser();
  };


  handleselectedFile = event => {
    this.setState({
      selectedFile: event.target.files[0],
      loaded: 0,
    })
  }


  handleUpload = () => {
    const data = new FormData()
    data.append('file', this.state.selectedFile)
    data.append('filename', this.state.selectedFile.name)

    const config = {
      headers: {
        'content-type': 'multipart/form-data'
      }
    };

    axios
      .post("/api/users/upload", data, config, {
        onUploadProgress: ProgressEvent => {
          this.setState({
            loaded: (ProgressEvent.loaded / ProgressEvent.total * 100),
          })
        },
      })
      .then(res => {
        console.log(res.statusText)
      })

  }

  render() {
    const { user } = this.props.auth;


    return (
      <div style={{ height: "75vh" }} className="container valign-wrapper">
        <div className="row">
          <div className="col s12 center-align">
            <h4>
              <b>Hey there,</b> {user.name.split(" ")[0]}
              <p className="flow-text grey-text text-darken-1">
                You are logged into a Human Resource data verification using blockchain{" "}
                <span style={{ fontFamily: "monospace" }}></span> app
              </p>
            </h4>
            <button
              style={{
                width: "150px",
                borderRadius: "3px",
                letterSpacing: "1.5px",
                marginTop: "1rem"
              }}
              onClick={this.onLogoutClick}
              className="btn btn-large waves-effect waves-light hoverable blue accent-3"
            >
              Logout
            </button>

            <FilePond server={
              {

                process: {
                  url: '/api/users/upload',
                  onload: (res) => {
                    // select the right value in the response here and return
                    console.log(res)
                    var values = JSON.parse(res)
                    console.log(values.SHA)
                    this.sha_change(values.SHA)
                  },
                  onerror: null
                }
              }} onupdatefiles={(fileItems) => {
                // Set current file objects to this.state
                this.setState({
                  files: fileItems.map(fileItem => fileItem.file)
                });
                console.log(fileItems)
              }}></FilePond>
            <div>
              SHA value= {this.state.SHA}
            </div>
          </div>
        </div>
      </div>
    );
  }
}

Dashboard.propTypes = {
  logoutUser: PropTypes.func.isRequired,
  auth: PropTypes.object.isRequired
};

const mapStateToProps = state => ({
  auth: state.auth,
});

export default connect(
  mapStateToProps,
  { logoutUser }
)(Dashboard);