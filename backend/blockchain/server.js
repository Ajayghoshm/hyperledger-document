//created by Team 4
//Ajayghosh M
//Tinu Mathew
//Altaf salim
//V Pranav


const express = require("express");
const mongoose = require("mongoose");
const bodyParser = require("body-parser");
const passport = require("passport");
const users = require("./routes/api/users");
var multer  = require('multer')
//const fileUploadMiddleware=require("./fileupload")
/*var busboy = require('connect-busboy');
const busboyBodyParser = require('busboy-body-parser');*/
const fileUpload = require('express-fileupload');
const fs=require("fs")


const app = express();

// Bodyparser middleware
app.use(
  bodyParser.urlencoded({
    extended: true
  })
);
app.use(bodyParser.json());

/*app.use(busboy());
app.use(busboyBodyParser());*/
app.use(fileUpload());
// DB Config
const db = require("./config/keys").mongoURI;

// Connect to MongoDB
mongoose 
  .connect(
    db,
    { useNewUrlParser: true }
  )
  .then(() => console.log("MongoDB successfully connected"))
  .catch(err => console.log(err));

// Passport middleware
app.use(passport.initialize());

// Passport config
require("./config/passport").pass(passport);

/*var storage = multer.diskStorage({
  destination: function (req, file, callback) {
      var dir = '/uploads';
      if (!fs.existsSync(dir)){
          fs.mkdirSync(dir);
      }
      callback(null, dir);
  },
  filename: function (req, file, callback) {
      callback(null, file.originalname);
  }
});
var upload = multer({dest:"./upedd/"}).array('files', 12);
app.post('/upload', function (req, res, next) {
  upload(req, res, function (err) {
      if (err) {
          return res.end("Something went wrong:(");
      }
      res.end("Upload completed.");
  });
})*/

// Routes
app.use("/api/users", users);

const port =8000;

app.listen(port, () => console.log(`Server up and running on port ${port} !`));
